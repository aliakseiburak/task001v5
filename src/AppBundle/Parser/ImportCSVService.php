<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/16/16
 * Time: 10:26 AM
 */

namespace AppBundle\Parser;

use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\ValidatorInterface;

use Ddeboer\DataImport\Exception as Exc;

use Ddeboer\DataImport\Workflow;
use Ddeboer\DataImport\Reader\CsvReader;
use AppBundle\Helpers\CSVServiceDoctrineWriter;
use Ddeboer\DataImport\Filter\CallbackFilter;
use Ddeboer\DataImport\ValueConverter\CallbackValueConverter;
use Ddeboer\DataImport\ItemConverter\MappingItemConverter;

/**
 * Class ImportCSVService
 * @package AppBundle\Parser
 */
class ImportCSVService extends Controller
{
    private $em;
    private $itemsCodes = array();
    private $batchSize = 20;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ImportCSVService constructor.
     * @param object EntityManager      $em
     * @param object ValidatorInterface $validator
     */
    public function __construct(EntityManager $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @param string $filename
     * @return \Ddeboer\DataImport\Result|object
     */
    public function getFileObject($filename)
    {
        // The SplFileObject class offers an object oriented interface for a file. //
        $fileObject = new \SplFileObject($filename);

        return $fileObject;
    }

    /**
     * Creates and configures the reader.
     * @param \SplFileObject $fileObject
     * @return CsvReader
     */
    public function getConfiguredCSVReader(\SplFileObject $fileObject)
    {
        $csvReader = new CsvReader($fileObject, ',');
        if ($csvReader === false) {
            throw new Exc\ReaderException('Failed create create Reader. Ensure that file still exists.');
        }
        // Number of the row that contains column header names. //
        $csvReader->setHeaderRowNumber(0);

        return $csvReader;
    }

    /**
     * Create custom DoctrineWriter that implements additional validation before database import.
     * @param array|string $index
     * @throws Exc\WriterException
     * @return CSVServiceDoctrineWriter
     */
    public function getConfiguredDoctrineWriter($index = null)
    {
        // If $index is set by command option, DoctrineWriter updates Product information.
        // In this case need to set a field to look for entities by. //
        $index = $index ? 'code' : null ;
        $doctrineWriter = new CSVServiceDoctrineWriter($this->em, 'AppBundle:Product', $this->validator, $index);

        if ($doctrineWriter === false) {
            throw new Exc\WriterException('Failed create create Writer. Ensure that file file still exists.');
        }

        // Whether to truncate the table first. //
        $doctrineWriter->disableTruncate();

        // Set number of entities that may be persisted before a new flush. //
        $doctrineWriter->setBatchSize($this->batchSize);

        return $doctrineWriter;
    }

    /**
     * Create the workflow using the reader.
     * @param $csvReader
     * @return Workflow
     */
    public function getWorkflow(CsvReader $csvReader)
    {
        $workflow = new Workflow($csvReader);

        return $workflow;
    }

    /**
     * Mapping items in comparison with Product entity properties.
     * @return mixed
     */
    public function getMappingConverter()
    {
        $converter = new MappingItemConverter();

        $converter->addMapping('Product Code', 'code')
            ->addMapping('Product Name', 'name')
            ->addMapping('Product Description', 'description')
            ->addMapping('Cost in GBP', 'cost')
            ->addMapping('Stock', 'stock')
            ->addMapping('Discontinued', 'discontinued');

        return $converter;
    }

    /**
     * Filter for --update option.
     * @param Workflow $workflow
     * @return mixed
     */
    public function addDetectItemChangesFilter($workflow)
    {
        $repository = $this->em->getRepository('AppBundle:Product');

        $workflow->addFilterAfterConversion(
            new CallbackFilter($this->getComparedProductItem($repository))
        );

        return $workflow;
    }

    /**
     * Correlates Product information from database with one from .csv data file.
     * Product accepted for UPDATE if mismatch is detected.
     * Also allows to import Product information if that is not present.
     * @param EntityRepository $repository
     * @return \Closure
     */
    private function getComparedProductItem($repository)
    {
        return
            function ($item) use ($repository) {
                $product = $repository->findOneBy(array('code' => $item['code']));

                if (!$product) {
                    // True allows to persist non-present Product data to database on update mode. //
                    return true;
                }

                return ($product->getName() != $item['name'] ||
                    $product->getDescription() != $item['description'] ||
                    $product->getCost() != $item['cost'] ||
                    $product->getStock() != $item['stock'] ||
                    is_object($product->getDiscontinued()) != is_object($item['discontinued']))
                    ? true : null;
            };
    }

    /**
     * Filter for --unique-check option.
     * @param $workflow
     * @return mixed
     */
    public function addUniqueCheckFilter($workflow)
    {
        $workflow->addFilter(
            new CallbackFilter($this->getUniqueProductItem())
        );

        return $workflow;
    }

    /**
     * Checks uniqueness of Product codes.
     * The data is accepted only if the function returns true.
     * @return \Closure
     */
    private function getUniqueProductItem()
    {
        return function ($item) {
            if ((count($this->itemsCodes) % $this->batchSize) == 0) {
                $this->itemsCodes = array();
            }

            return in_array(
                $item['Product Code'],
                $this->itemsCodes
            ) ? null : $this->itemsCodes[] = $item['Product Code'];
        };
    }

    /**
     * Configurable Ddeboer library for import Product information from .csv file to DB.
     * @param $workflow
     * @param $doctrineWriter
     * @param $converter
     * @return Result $result
     */
    public function configureWorkflow(Workflow $workflow, CSVServiceDoctrineWriter $doctrineWriter, MappingItemConverter $converter)
    {

        $workflow->setSkipItemOnFailure(true);
        // A writer takes a filtered and converted item, and writes that to a database. //
        $result = $workflow->addWriter($doctrineWriter)
            // Mapping items. //
            ->addItemConverter($converter);

        // Implementing business rules. //
        $result->addValueConverter('discontinued', new CallbackValueConverter(
                function ($item) {

                    return $item ? new \DateTime('now') : null;
                }
            )
        );
        $result->addValueConverter('stock', new CallbackValueConverter('intval'))
            // Escape non-numeric symbols in a Product cost field. //
            ->addValueConverter('cost', new CallbackValueConverter(
                function ($item) {

                    return filter_var(
                        $item,
                        FILTER_SANITIZE_NUMBER_FLOAT,
                        FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND
                    );
                }
            )
        );

        return $result;
    }

    /**
     * Process the workflow.
     * @param $result
     * @return mixed
     */
    public function processItemsImport($result)
    {
        return $result->process();
    }

    /**
     * Set number of entities that may be persisted before a new flush
     *
     * @param int $size
     * @return $this
     */
    public function setBatchSize($size)
    {
        $this->batchSize = $size;

        return $this;
    }
}
