<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/23/16
 * Time: 10:59 AM
 */

namespace AppBundle\Helpers;

use Symfony\Component\Validator\ValidatorInterface;
use Ddeboer\DataImport\Exception\ValidationException;

use AppBundle\Entity\Product;

/**
 * Class CSVServiceEntityValidator
 * @package AppBundle\Helpers
 */
class CSVServiceEntityValidator
{
    private $validator;

    private $throwExceptions = false;

    private $violations = array();

    /**
     * CSVServiceEntityValidator constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param bool $flag
     */
    public function throwExceptions($flag = true)
    {
        $this->throwExceptions = $flag;
    }

    /**
     * @return array
     */
    public function getViolations()
    {
        return $this->violations;
    }

    /**
     * @param Product $item
     * @return bool
     * @throws ValidationException
     */
    public function filter(Product $item)
    {
        $list = $this->validator->validate($item);
        if (count($list) > 0) {
            if ($this->throwExceptions) {
                throw new ValidationException($list, 'database err:');
            }
        }

        return 0 === count($list);
    }

    /**
     * {@inheritdoc}
     */
    public function getPriority()
    {
        return 256;
    }
}
