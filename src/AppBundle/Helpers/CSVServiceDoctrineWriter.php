<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/22/16
 * Time: 11:15 AM
 */

namespace AppBundle\Helpers;

use Doctrine\ORM\EntityManager;
use Ddeboer\DataImport\Writer\DoctrineWriter;
use Ddeboer\DataImport\Exception;

/**
 * Class CSVServiceDoctrineWriter
 *
 * A butch doctrine writer extends original Ddeboer's DoctrineWriter
 * in order to make a Product entity validation.
 *
 * @package AppBundle\Helpers
 */
class CSVServiceDoctrineWriter extends DoctrineWriter
{
    private $validator;

    /**
     * CSVServiceDoctrineWriter constructor.
     *
     * @param EntityManager     $entityManager
     * @param string            $entityName
     * @param array|string      $index
     * @param array|null|string $validator
     * @param array|string      $index
     */
    public function __construct(EntityManager $entityManager, $entityName, $validator, $index)

    {
        $this->validator = $validator;
        parent::__construct($entityManager, $entityName, $index);
    }

    /**
     * {@inheritdoc}
     */
    public function writeItem(array $item)
    {
        $this->counter++;
        $entity = $this->findOrCreateItem($item);

        $this->loadAssociationObjectsToEntity($item, $entity);
        $this->updateEntity($item, $entity);

        // Entity validation (not present in parent class). //
        $filter = $this->entityValidation($entity);

        if (false !== $filter) {
            $this->entityManager->persist($entity);
        }

        if (($this->counter % $this->batchSize) == 0) {
            $this->flushAndClear();
        }

        return $this;
    }

    /**
     * Validates entity following ddeboer's mechanism.
     *
     * @param object $entity
     * @return bool
     * @throws Exception\ValidationException
     */
    public function entityValidation($entity)
    {
        $validation = new CSVServiceEntityValidator($this->validator);
        $validation->throwExceptions();

        return $validation->filter($entity);
    }
}
