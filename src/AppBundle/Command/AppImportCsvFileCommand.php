<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Style\SymfonyStyle;

use Ddeboer\DataImport\Exception as Exc;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * Class AppImportCsvFileCommand
 * Imports data from csv file to DB.
 * @package AppBundle\Command
 */
class AppImportCsvFileCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:import-csv-file')
            ->addArgument('filename', InputArgument::REQUIRED, 'Absolute path to the file and its name')
            ->addOption(
                'unique-check',
                null,
                InputOption::VALUE_NONE,
                'Disable unique validation for Product code in a file'
            )
            ->addOption(
                'update',
                null,
                InputOption::VALUE_NONE,
                'Update Product information already present in the database'
            )
            ->setDescription('Loads .csv file containing product items into database')
            ->setHelp(
                <<<'EOF'
The <info>%command.name%</info> command parses .csv file, invoke valid lines, implements business
rules and inserts product items into a database. A file should be indicated as follows:

  <info>php %command.full_name% path/to/the/file/filename.csv</info>

Pay attention that the following headers must be present in the file:

  <info>| Product Code | Product Name | Product Description | Cost in GBP | Stock | Discontinued |</info>

Use flag <info>--unique-check</info> only if you are sure that file contains no doubles for Product code.
Use flag <info>--update</info> for updating existing Product information and insert new one.
Also you can use both.

At the end of processing command outputs a report with following information:
- total valid lines grabbed from file;
- total validation exceptions (including business rules, product code repetitions);
- total database inserts.

EOF
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('filename');
        $update = $input->getOption('update');
        $service = $this->getContainer()->get('app.import_csv_service');
        //** @var SymfonyStyle $io */
        $io = new SymfonyStyle($input, $output);

        $io->title('******************** File loading started ********************');
        try {
            $fileObject = $service->getFileObject($filename);
            $csvReader = $service->getConfiguredCSVReader($fileObject);
            // @param indicates database UPDATE query. //
            $doctrineWriter = $service->getConfiguredDoctrineWriter($update);
            $workflow = $service->getWorkflow($csvReader);

            if ($update) {
                $workflow = $service->addDetectItemChangesFilter($workflow);
            }

            $converter = $service->getMappingConverter();

            if ($input->getOption('unique-check')) {
                $workflow = $service->addUniqueCheckFilter($workflow);
            }

            $result = $service->configureWorkflow($workflow, $doctrineWriter, $converter);
            $result = $service->processItemsImport($result);

            foreach ($result->getExceptions() as $rowIndex => $exception) {
                $violations = $exception->getViolations();
                $output->writeln(sprintf("File line: %u exception %s", $rowIndex + 1, (string)$violations));

            }

            if ($update) {
                $io->note('Total database non-present lines processed: '.$result->getTotalProcessedCount());
            } else {
                $io->note('Total valid lines processed: '.$result->getTotalProcessedCount());
            }
            $io->note('Total validation exceptions: '.$result->getErrorCount());
            $io->success('Total database inserts:      '.$result->getSuccessCount());

        } catch (Exc\ReaderException $e) {
            $io->error($e->getMessage());
        } catch (Exc\WriterException $e) {
            $io->error($e->getMessage());
        } catch (\RuntimeException $e) {
            $io->error('Failed to open file. Ensure that file exists and is not empty.');
        } catch (ContextErrorException $e) {
            $io->error('File format: '.$e->getMessage());
        } catch (UniqueConstraintViolationException $e) {
            $io->error($e->getMessage());
            $io->note('To escape this error use:  ');
            $io->block('      --unique-check option for the first file upload.');
            $io->block('      --update option for update existing Product information and insert new one.');
        }
    }
}
