<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/24/16
 * Time: 8:08 PM
 */

namespace AppBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

use AppBundle\Entity\Product;
use AppBundle\Helpers\CSVServiceEntityValidator;

/**
 * Class CSVServiceEntityValidatorTest
 * @package AppBundle\Tests
 */
class CSVServiceEntityValidatorTest extends KernelTestCase
{
    private $container;
    private $validator;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer()->get('validator');
        $this->validator = new CSVServiceEntityValidator($this->container);
    }

    /**
     * @dataProvider provideValidEntities
     * @param object $product
     */
    public function testValidEntityValidation($product)
    {
        $result = $this->validator->filter($product);
        $this->assertTrue($result, 'Passes validation');
    }

    /**
     * {@inheritDoc}
     */
    public function provideValidEntities()
    {
        return array(
            // Product stock & cost validation. //
            [(new Product())->setName('Pr1')->setDescription('prDescr')->setCode(001)->setCost('1')->setStock(11)],
            [(new Product())->setName('Pr1')->setDescription('prDescr')->setCode(001)->setCost('5')->setStock(1)],
            [(new Product())->setName('Pr2')->setDescription('prDescr')->setCode(002)->setCost('1199')->setStock(1)],
        );
    }

    /**
     * @dataProvider provideInvalidEntities
     * @param object $product
     */
    public function testInvalidEntityValidation($product)
    {

        $result = $this->validator->filter($product);
        $this->assertFalse($result, 'Validation throws an Exception');
    }

    /**
     * {@inheritDoc}
     */
    public function provideInvalidEntities()
    {
        return array(
            // Product stock & cost validation. //
            [(new Product())->setName('Pr1')->setDescription('prDescr')->setCode(001)->setCost('4.99')->setStock(9)],
            // Product cost validation. //
            [(new Product())->setName('Pr2')->setDescription('prDescr')->setCode(002)->setCost('1200.07')->setStock(9)],
            // blank Product name. //
            [(new Product())->setName('')->setDescription('prDescr')->setCode(003)->setCost('999')->setStock(9)],
            // blank Product description. //
            [(new Product())->setName('Pr4')->setDescription('')->setCode(004)->setCost('999')->setStock(9)],
            // blank Product code cannot test, unable to create entity (UniqueEntity constraint). //
            // blank Product cost. //
            [(new Product())->setName('Pr6')->setDescription('prDescr')->setCode(006)->setCost('')->setStock(9)],
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        $this->container = null;
    }
}