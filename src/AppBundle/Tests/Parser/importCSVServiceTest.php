<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/24/16
 * Time: 7:59 PM
 */

namespace AppBundle\Tests\Parser;

use Ddeboer\DataImport\Filter\FilterInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use AppBundle\Parser\ImportCSVService;
use Ddeboer\DataImport\ItemConverter\MappingItemConverter;
use AppBundle\Entity\Product;

/**
 * Class CSVServiceEntityValidatorTest
 * @package AppBundle\Tests\Parser
 */
class CSVServiceEntityValidatorTest extends KernelTestCase
{
    private $service;
    private $em;
    private $workflow;

    /**
     * Nessesary objects for a ImportCSVService __constructor().
     */
    public function setUp()
    {
        $this->em = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->setMethods(
                array('getRepository'))
            ->disableOriginalConstructor()
            ->getMock();
        $validator = $this->getMockBuilder('Symfony\Component\Validator\Validator')
            ->disableOriginalConstructor()
            ->getMock();

        $this->service = new ImportCSVService($this->em, $validator);
    }

    /**
     * Tests wrong path to a file.
     */
    public function testNotExistFileLoad()
    {
        $this->setExpectedException('RuntimeException');
        $this->service->getFileObject('no/exist/file.csv');
    }

    /**
     * Tests comlettely empty file.
     */
    public function testEmptyFileLoad()
    {
        $this->setExpectedException('RuntimeException');
        $this->service->getFileObject('../Fixtures/data_empty.csv');
    }

    /**
     * SplFileObject doesn't check a ile format.
     */
    public function testInvalidFileLoad()
    {
        $file = new \SplFileObject('src/AppBundle/Tests/Fixtures/data_wrong_format.csv');
        $result = $this->service->getConfiguredCSVReader($file);
        $this->assertTrue(is_object($result), 'Object created');
    }

    /**
     * @dataProvider provideMappingInput
     * @param $input
     */
    public function testWrongFileFormat($input)
    {
        $converter = new MappingItemConverter();
        $converter->addMapping('Product Code', 'code')
            ->addMapping('Product', 'name');

        $output = $converter->convert($input);

        $expected = array(
            'code' => '00001',
            'Product Name' => 'Sony PS3',
        );

        $this->assertEquals($expected, $output);
    }

    public function provideMappingInput()
    {
        return array(
            array(
                array(
                    'Product Code' => '00001',
                    'Product Name' => 'Sony PS3',
                ),
            ),
        );
    }

    public function testDetectItemChangesCall()
    {
        $this->workflow = $this->getMockBuilder('Workflow')
            ->setMethods(
                array('addFilter', 'addFilterAfterConversion'))
            ->disableOriginalConstructor()
            ->getMock();

        $this->workflow->expects($this->once())
            ->method('addFilterAfterConversion')
            ->with($this->isInstanceOf(FilterInterface::class));

        $this->service->addDetectItemChangesFilter($this->workflow);

    }

    public function testComparedProductItemReturn()
    {
        $repository = $this->em->getRepository('AppBundle:Product');

        $method = new \ReflectionMethod(ImportCSVService::class, 'getComparedProductItem');
        $method->setAccessible(true);
        $result = $method->invoke($this->service, $repository);

        $this->assertInstanceOf(\Closure::class, $result);
    }

    /**
     * @dataProvider provideInvalidProductDataComparison
     * @param $item
     * @param $product
     */
    public function testInvalidProductDataComparison($item, $product)
    {
        $result = ( $product->getName() != $item['name'] ||
                    $product->getDescription() != $item['description'] ||
                    $product->getCost() != $item['cost'] ||
                    $product->getStock() != $item['stock'] ||
                    is_object($product->getDiscontinued()) != is_object($item['discontinued']) )
            ? true : false;

        $this->assertTrue($result);
    }

    public function provideInvalidProductDataComparison()
    {
        return array(
            // Product != product. //
            array(
                array('name' => 'Pr1', 'description' => 'Product', 'stock' => '1', 'cost' => '100', 'discontinued' => new \DateTime()),
                (new Product())->setName('Pr1')->setDescription('product')->setStock(1)->setCost(100)->setDiscontinued(new \DateTime())),
            // DateTime() != 'yes'. //
            array(
                array('name' => 'Pr2', 'description' => 'Product', 'stock' => '2', 'cost' => '200', 'discontinued' => 'yes'),
                (new Product())->setName('Pr2')->setDescription('Product')->setStock(2)->setCost(200)->setDiscontinued(new \DateTime())),
        );
    }

    /**
     * @dataProvider provideValidProductDataComparison
     * @param $item
     * @param $product
     */
    public function testValidProductDataComparison($item, $product)
    {
        $result = ( $product->getName() != $item['name'] ||
            $product->getDescription() != $item['description'] ||
            $product->getCost() != $item['cost'] ||
            $product->getStock() != $item['stock'] ||
            is_object($product->getDiscontinued()) != is_object($item['discontinued']) )
            ? true : false;
        var_dump($result);
        $this->assertFalse($result);
    }

    public function provideValidProductDataComparison()
    {
        return array(
            array(
                array('name' => 'Pr3', 'description' => 'Product', 'stock' => '3', 'cost' => '300', 'discontinued' => new \DateTime()),
                (new Product())->setName('Pr3')->setDescription('Product')->setStock(3)->setCost(300)->setDiscontinued(new \DateTime())),
            // 4 == 00004, 400 == 400.000 //
            array(
                array('name' => 'Pr4', 'description' => 'Product', 'stock' => '4', 'cost' => '400', 'discontinued' => new \DateTime()),
                (new Product())->setName('Pr4')->setDescription('Product')->setStock(00004)->setCost(400.000)->setDiscontinued(new \DateTime())),
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        $this->service = null;
    }
}
